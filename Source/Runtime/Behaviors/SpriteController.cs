using UnityEngine;


namespace Manganese.SpriteKit {
  public class SpriteController : MonoBehaviour {
		public Texture2D texture;

		public void Start() {
			Debug.Log("started");

			Spritesheet.CreateAndTile(this.texture);
		}
  }
}