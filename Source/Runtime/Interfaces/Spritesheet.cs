using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;


namespace Manganese.SpriteKit {
  public class Spritesheet {
    private static IEnumerable<uint> GetFrameSizeAxisCandidates(uint size) {
      for (uint i = 5; i * i <= size; i++) {
        if (0 == (size % i)) {
          yield return i;

          if (i != (size / i)) {
            yield return size / i;
          }
        }
      }
    }

    private static Vector2Int GetFrameSize(Texture2D texture) {
      // Factorize width and height for candidate axis sizes
      var widthCandidates = GetFrameSizeAxisCandidates((uint) texture.width);
      var heightCandidates = GetFrameSizeAxisCandidates((uint) texture.height);

      // Mash the sets of candidate axis sizes to get candidate sizes as Vector2Ints
      var candidateSizes = new HashSet<Vector2Int>();

      foreach (var widthCandidate in widthCandidates) {
        foreach (var heightCandidate in heightCandidates) {
          var candidateSize = new Vector2Int((int) widthCandidate, (int) heightCandidate);

          candidateSizes.Add(candidateSize);
        }
      }

      var candidateSizeErrors = new Dictionary<Vector2Int, float>();

      Debug.Log("sprite size [" + texture.width + "w " + texture.height + "h]");

      foreach (var candidateSize in candidateSizes) {
        Debug.Log("calculating error from candidate size: [" + candidateSize.x + "x " + candidateSize.y + "y]");
        var frameColumns = texture.width / candidateSize.x;
        var frameRows = texture.height / candidateSize.y;
        var frameCount = frameColumns * frameRows;

        // Calculate opacity ratio for each frame at candidate size
        var frameOpacityRatios = new List<float>();

        for (var y = 0; y < texture.height; y += candidateSize.y) {
          for (var x = 0; x < texture.width; x += candidateSize.x) {
            var framePixels = texture.GetPixels(x, y, candidateSize.x, candidateSize.y);
            var frameOpacityRatio = framePixels.Average(framePixel => framePixel.a);
            frameOpacityRatios.Add(frameOpacityRatio);
          }
        }

        // Filter out frame opacity ratios with value of zero
        frameOpacityRatios = (
          from frameOpacityRatio in frameOpacityRatios
          where frameOpacityRatio != 0F
          select frameOpacityRatio
        ).ToList();

        // Select minimum and maximum remaining opacity ratios
        var minimumFrameOpacityRatio = frameOpacityRatios.Min();
        var maximumFrameOpacityRatio = frameOpacityRatios.Max();

        // Calculate error as difference between maximum and minimum non-zero frame opacity ratios
        var error = maximumFrameOpacityRatio - minimumFrameOpacityRatio;

        candidateSizeErrors[candidateSize] = error;
      }

      // Select candidate size with lowest error
      return candidateSizeErrors.OrderBy(pair => pair.Value).First().Key;
    }

    private static uint GetFrameCount(Texture2D texture, Vector2Int frameSize) {
      var frameColumns = texture.width / frameSize.x;
      var frameRows = texture.height / frameSize.y;
      var maximumFrameCount = frameColumns * frameRows;

      for (var frameIndex = maximumFrameCount - 1; frameIndex >= 0; frameIndex--) {
        var column = frameIndex % frameColumns;
        var row = frameRows - (frameIndex / frameRows) + 1;
        var framePixels = texture.GetPixels((int) column * frameSize.x, (int) row * frameSize.y, frameSize.x, frameSize.y);
        var isEmptyFrame = framePixels.All(pixel => pixel.a == 0F);

        if (!isEmptyFrame) {
          return (uint) frameIndex + 1;
        }
      }

      return 0;
    }

    public static Spritesheet CreateAndTile(Texture2D texture) {
      // Frame size
      var frameSize = GetFrameSize(texture);

      // Frame count
      var frameCount = GetFrameCount(texture, frameSize);

      // Build sprite sheet
      var spritesheet = new Spritesheet(texture);
      spritesheet.frameSize = frameSize;
      spritesheet.frameCount = frameCount;

      return spritesheet;
    }

    // Texture
    public Texture2D texture;

    // Frame size
    public uint frameWidth;
    public uint frameHeight;
    public Vector2Int frameSize {
      get {
        return new Vector2Int((int) this.frameWidth, (int) this.frameHeight);
      }
      set {
        this.frameWidth = (uint) value.x;
        this.frameHeight = (uint) value.y;
      }
    }

    // Frame count
    public uint frameCount;


    // Constructor
    public Spritesheet(Texture2D texture) {
      this.texture = texture;
    }
  }
}